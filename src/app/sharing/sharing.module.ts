import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomDatePipe } from 'src/assets/pipes/custom-date.pipe';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [CustomDatePipe],
  exports: [
    CustomDatePipe,
    TranslateModule
  ],
  imports: [
    CommonModule,
    TranslateModule
  ]
})
export class SharingModule { }
