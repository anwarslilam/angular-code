export class Certificat{
    id: number;
    name: string;
    dateObtained: Date;
    company: string;
    description: string;

}