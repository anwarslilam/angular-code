export class Interview {
   id: number;
   title: number;
   dateInterview: Date;
   result: string;
   resultDescription: string;
   evaluatorEmail: string;
   evaluatorName: string;
   evaluatorId: string;
   candidatId: number;
   offerId:number;
   entrepriseId:number;
   createdByWorkflow:boolean;
}
