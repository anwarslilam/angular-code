export class Critere{
    id: number;
    name: string;
    weight: number;
    minExp: number;
    maxExp: number;
}