export class Diplome{
    id: number;
    name: string;
    school: string;
    dateGraduation: Date;
    country: string;
    city: string;

}