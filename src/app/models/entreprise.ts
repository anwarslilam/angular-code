export class Entreprise{
    id: number;
    name: string;
    email: string;
    phone: string;
    fax: string;
    country: string;
    city: string;
    address: string;
    postalCode: string;
    website: string;
    statut: string;
    description: string;
    createdDate: Date;
}