import { Certificat } from "./certificat";
import { Diplome } from "./diplome";
import { Experiance } from "./experience";
import { Interview } from "./interview";
import { Skill } from "./skill";

export class Candidature{
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    phone: string;
    applyDate: Date;
    country: string;
    city: string;
    yearsExp: number;
    status: string;
    score: number;
    hiring: string;
    interviews: Array<Interview> = [];
    diplomes: Array<Diplome> = [];
    certificats: Array<Certificat> = [];
    skills: Array<Skill> = [];
    experiences: Array<Experiance> = [];
}
