import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { EmployeeDashboardComponent } from './employee-dashboard/employee-dashboard.component';
import { MorrisJsModule } from 'angular-morris-js';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HrDashboardComponent } from './hr-dashboard/hr-dashboard.component';
import { SharingModule } from 'src/app/sharing/sharing.module';

@NgModule({
  declarations: [DashboardComponent, AdminDashboardComponent,HrDashboardComponent, EmployeeDashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MorrisJsModule,
    FormsModule,
    ReactiveFormsModule,
    SharingModule

  ]
})
export class DashboardModule { }
