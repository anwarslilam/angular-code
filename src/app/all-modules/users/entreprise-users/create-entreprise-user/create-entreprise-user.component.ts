import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";


import { ToastrService } from "ngx-toastr";
import { Entreprise } from "src/app/models/entreprise";
import { User } from "src/app/models/user";
import { EntrepriseService } from "src/app/services/entreprise.service";
import { KeycloakSecurityService } from "src/app/services/keycloak-security.service";
import { RoleService } from "src/app/services/role.service";
import { Role } from "src/app/models/role";

@Component({
  selector: "app-create-entreprise-user",
  templateUrl: "./create-entreprise-user.component.html",
  styleUrls: ["./create-entreprise-user.component.css"],
})
export class CreateEntrepriseUserComponent implements OnInit {

  entreprise = new Entreprise();
  id_entreprise : any;
  user_roles : Array<any> = [];
  user = new User();
  roles : any;
  errorMessage: any;
  
  constructor(
    private router: Router,
    private toastr: ToastrService,

    private entrepriseService : EntrepriseService,
    private keycloakSecurityService : KeycloakSecurityService,
    private roleService : RoleService,
  ) {}

  ngOnInit() {
    //get all roles
    this.getRoles();
  }



  //getting all roles
  getRoles(){
    this.roleService.getRoles().subscribe(
      data => {
        this.roles = data;
        this.roles = this.roles.filter(r => r.name != "app-manager");
      },
      err => {
        this.errorMessage = err;
        console.log('error getting roles ! ', err);
      }
    );
  }

  //setting user roles
  onChange(role:Role, isChecked: boolean) {
    if(isChecked) {
      this.user_roles.push(role);
    } else {
        var i;
        for (i = 0; i < this.user_roles.length; i++) {
          if (this.user_roles[i].id === role.id) {
              break;
          }
      }
      this.user_roles.splice(i,1);
    }
  }

  //generating password
  generatePassword() {

    let availableCharacters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-,.'
    availableCharacters.split('')
    const generatedPassword = []

    for (let i = 0; i < 8; i += 1) {
      const max = availableCharacters.length
      const ran = Math.random()
      const idx = Math.floor(ran * (max + 1))

      generatedPassword.push(availableCharacters[idx])

    }

    this.user.password = generatedPassword.join('')
  }


  //post method function for user form

  addEntrepriseUser(){
    this.toastr.info("Wait ...","Info",{
      disableTimeOut: false,
      tapToDismiss: true,
      closeButton: true,
    });
    this.generatePassword();
    this.entreprise.id = this.keycloakSecurityService.userInformations.entreprise;
    this.entrepriseService
    .CreateEntrepriseUser(this.user, this.entreprise, this.user_roles).subscribe(
      response => {
        this.toastr.clear();
        this.toastr.success(response["message"], "Success");
        this.router.navigate(["/entreprise/users/entreprise-users"]);
      },
      err => {
        this.errorMessage = err;
        this.toastr.clear();
        this.toastr.warning(err.error.message, "Warning!");
      });
  }


}
