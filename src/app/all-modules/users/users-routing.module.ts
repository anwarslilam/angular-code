import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';
import { EntrepriseUsersComponent } from './entreprise-users/entreprise-users.component';
import { AfterLoginEntrepriseManagerAcceptedService } from 'src/app/services/guards/after-login-entreprise-manager-accepted.service';
import { CreateEntrepriseUserComponent } from './entreprise-users/create-entreprise-user/create-entreprise-user.component';
import { EditEntrepriseUserComponent } from './entreprise-users/edit-entreprise-user/edit-entreprise-user.component';

const routes: Routes = [
  {
    path:"",
    component:UsersComponent,
    children:[
      {
        path:"entreprise-users",
        canActivate: [AfterLoginEntrepriseManagerAcceptedService],
        component:EntrepriseUsersComponent
      },
      {
        path:"create-entreprise-user",
        canActivate: [AfterLoginEntrepriseManagerAcceptedService],
        component:CreateEntrepriseUserComponent
      },
      {
        path:"edit-entreprise-user/:id",
        canActivate: [AfterLoginEntrepriseManagerAcceptedService],
        component:EditEntrepriseUserComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
