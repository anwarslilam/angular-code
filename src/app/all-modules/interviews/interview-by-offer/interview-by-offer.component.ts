import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Interview } from 'src/app/models/interview';
import { Offer } from 'src/app/models/offer';
import { User } from 'src/app/models/user';
import { EntrepriseService } from 'src/app/services/entreprise.service';
import { KeycloakSecurityService } from 'src/app/services/keycloak-security.service';
import { InterviewService } from '../interview.service';
import { ToastrService } from 'ngx-toastr';
import { Candidature } from 'src/app/models/candidature';
declare const $: any;

@Component({
  selector: 'app-interview-by-offer',
  templateUrl: './interview-by-offer.component.html',
  styleUrls: ['./interview-by-offer.component.css']
})
export class InterviewByOfferComponent implements OnInit {

  candidats0: any = [];
  candidats: any = [];
  theOfferId = this.route.snapshot.params['id'];
  theCandidatId: number;
  offer: Offer = new Offer();
  time = {hour: 0, minute: 0};
  id_entreprise : number;
  rangeColumn = [];
  selectedInterview: Interview = new Interview();
  currentInterview: Interview = new Interview();
  minDate = new Date();
  selectedCandidat: Candidature = new Candidature();
  Arr = Array; //Array type captured in a variable
  userConnected: string;

  allUsers: User[] = [];

  constructor(
    private route: ActivatedRoute,
    private interviewService: InterviewService,
    private keycloakSecurityService : KeycloakSecurityService,
    private entrepriseService : EntrepriseService,
    private toastr: ToastrService,
    
  ) {}

  ngOnInit(): void {
    this.getOffer();
    this.listCandidatByOfferAndStatus();
    this.id_entreprise = this.keycloakSecurityService.userInformations.entreprise;
    this.listUsersOfEntreprise();
    this.userConnected = this.keycloakSecurityService.userInformations.sub;
    console.log( this.keycloakSecurityService.userInformations.sub);
  }

  getOffer(){
    this.interviewService.getOffer(this.theOfferId).subscribe(
      data=>{
        this.offer = data;
      },
      err=>{
        console.log('error generated'+ err);
      }
    )
  }
  // get interview by id
  getInteview(id: number){
    this.interviewService.getInterview(id).subscribe(
      data=>{
        this.currentInterview = data;
      },
      err=>{
        console.log('error generated'+ err);
      }
    )
  }

  // get all candidature apply to offer and have status 'approved'
  listCandidatByOfferAndStatus(){
    this.interviewService.getCandidatByOfferAndStatus(this.theOfferId,'approved').subscribe(
      data=>{
        this.candidats = data;
        this.candidats.sort(this.compareScore);
        this.candidats.forEach(candidat => {
          candidat.interviews.sort(this.compareId);
        });
        this.candidats0 = this.candidats;
        this.maxColumnOfInterview();
      },
      err=>{
        console.log('error generated'+ err);
      }
    )
    
  }

  compareDate = function (interview1, interview2) {  
    var emp1Date = new Date(interview1.dateInterview).getTime();  
    var emp2Date = new Date(interview2.dateInterview).getTime();  
    return emp1Date > emp2Date ? 1 : -1;    
  }
  compareId = function (interview1, interview2) {   
    return interview1.id > interview2.id ? 1 : -1;    
  }
  compareScore = function (candidat1, candidat2) {  
    if (candidat1.score > candidat2.score) { return -1; }  
    if (candidat1.score < candidat2.score) {return 1; }  
    return 0;  
} 


  // get all user the entreprise
  listUsersOfEntreprise(){
    this.entrepriseService.getEntrepriseUsers(this.id_entreprise).subscribe(
      data => {
        this.allUsers = data;
      },
      err => {
        console.log('error generated'+ err);
      }
    )}

    // on click on add interview button
    handleModalAddInterveiw(candidatId: number){
      this.theCandidatId= candidatId;
      $('input[name=title]').val('');
      $('#dateInterview').val('');
      $('#evaluator').prop('selectedIndex',-1);
      this.time = {hour: 0, minute: 0};
    }

    // add new interview
    addInterview(newInterview: any){
      const interview = new Interview();
      const evaluator: User = this.allUsers.find(u=>u.id == newInterview.evaluatorId);
      const date: Date = newInterview.date;
      date.setHours(this.time.hour);
      date.setMinutes(this.time.minute);
      interview.title= newInterview.title;
      interview.dateInterview= date;
      interview.offerId = this.theOfferId;
      interview.entrepriseId = this.id_entreprise;
      interview.candidatId=this.theCandidatId;
      interview.evaluatorEmail= evaluator.email;
      interview.evaluatorId= evaluator.id;
      interview.evaluatorName= evaluator.firstName +" "+ evaluator.lastName;
      interview.createdByWorkflow = false;

      this.interviewService.addInterview(interview, this.id_entreprise).subscribe(
        data=>{
          $('#add_interview').modal('hide');
          this.toastr.success("L'entretien a été creé avec succès");
          this.listCandidatByOfferAndStatus();
        },
        err=>{
          $('#add_interview').modal('hide');
          this.toastr.error("une erreur a été generée, essayer plus tard");
        }
      )
      
    }

    // on click on edit interview
    dateForEdit: Date;
    handleModalEditInterveiw(interview: Interview, candidatId: number){
      console.log(interview);
      this.theCandidatId = candidatId;
      this.selectedInterview = interview;
      if(interview.dateInterview == null){
        this.dateForEdit = null;
        this.time = {hour: 0, minute: 0};
      }
      else{
        const date: Date = new Date(interview.dateInterview);
        this.time = {hour: date.getHours(), minute: date.getMinutes()};
        this.dateForEdit = new Date(this.selectedInterview.dateInterview);
      }
      //$('select[name=evaluator]').val(interview.evaluatorId).change();
      //$(`#eval option[value='${interview.evaluatorId}']`).prop('selected', true);

    }

    // edit interview
    editInterview(editedInterview: any){
      const evaluator: User = this.allUsers.find(u=>u.id == editedInterview.evaluator);
      const date: Date = new Date(editedInterview.date);
      date.setHours(this.time.hour);
      date.setMinutes(this.time.minute);
      this.selectedInterview.title = editedInterview.title;
      this.selectedInterview.evaluatorId= evaluator.id;
      this.selectedInterview.dateInterview= date;
      this.selectedInterview.candidatId = this.theCandidatId;
      this.selectedInterview.evaluatorEmail= evaluator.email;
      this.selectedInterview.evaluatorName= evaluator.firstName +" "+ evaluator.lastName;
      this.selectedInterview.offerId = this.theOfferId;
      this.selectedInterview.entrepriseId = this.id_entreprise;

      this.interviewService.editInterview(this.selectedInterview).subscribe(
        data=>{
          $('#edit_interview').modal('hide');
          $('#edit_date').modal('hide');
          this.toastr.success("L'entretien a été Modifié avec succès");
          this.listCandidatByOfferAndStatus();
        },
        err=>{
          $('#edit_interview').modal('hide');
          $('#edit_date').modal('hide');
          this.toastr.error("une erreur a été generée, essayer plus tard");
        }
      )
    }

    maxColumnOfInterview(){
      let max = 0;
      this.rangeColumn= [];
      this.candidats.forEach(candidat => {
        if (candidat.interviews.length > max) {
          max = candidat.interviews.length;
        }
      });
      for(var i=0; i<max; i++) { 
        this.rangeColumn.push(i+1);
      }
    }

    // delete interview
    deleteInterview(){
      const id = this.selectedInterview.id;
      this.interviewService.deleteInterview(id).subscribe(
        data=>{
          (<any>$('#delete_interview')).modal('hide');
          this.toastr.success("L'entretien a été supprimé avec succès");
          this.listCandidatByOfferAndStatus();
        },
        err=>{
          (<any>$('#delete_interview')).modal('hide');
          this.toastr.error("une erreur a été generée, essayer plus tard");
        }
      )
    }

    // edit result and description_result of interview
    edit_result(res: any){
      this.selectedInterview.result = res.result;
      this.selectedInterview.resultDescription = res.resultDescription;
      this.interviewService.editResult(this.selectedInterview).subscribe(
        data=>{
          $('#edit_result').modal('hide');
          this.toastr.success("Le resultat a été Modifié avec succès");
          this.listCandidatByOfferAndStatus();
        },
        err=>{
          $('#edit_result').modal('hide');
          this.toastr.error("une erreur a été generée, essayer plus tard");
        }
      )
    }

    // edit hiring decision
    edit_hiring(){
      this.interviewService.editHiring(this.selectedCandidat).subscribe(
        data=>{
          $('#edit_hiring').modal('hide');
          this.toastr.success("La decision d'embauche a été Modifié avec succès");
          this.listCandidatByOfferAndStatus();
        },
        err=>{
          $('#edit_hiring').modal('hide');
          this.toastr.error("une erreur a été generée, essayer plus tard");
        }
      )
    }

    // the roles
    isEntrepriseManager() {
      return this.keycloakSecurityService.isEntrepriseManager();
    }
  
    isEntrepriseHR() {
      return this.keycloakSecurityService.isEntrepriseHR();
    }
    isLogin(){
      return this.keycloakSecurityService.login();
    }

    searchByName(key){
      this.candidats = this.candidats0;
      this.candidats = this.candidats.filter(c => (c.firstName+" "+c.lastName).toLowerCase().includes(key));
    }
}
