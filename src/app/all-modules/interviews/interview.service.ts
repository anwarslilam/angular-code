import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Interview } from 'src/app/models/interview';
import { Offer } from 'src/app/models/offer';
import { WorkflowInterview } from 'src/app/models/workflow-interview';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InterviewService {

  private baseUrl = environment.baseUrl;

  constructor(private httpClient: HttpClient) { }

  // get one offer
  getOffer(id: number): Observable<Offer>{
    return this.httpClient.get<Offer>(this.baseUrl+`/offers/${id}`);
  }

  getInterview(id: number): Observable<Interview>{
    return this.httpClient.get<Interview>(this.baseUrl+`/interviews/${id}`);
  }

  // get offers of one entreprise
  // getOffersByEntreprise(id: number){
  //   return this.httpClient.get<Offer[]>(this.baseUrl+`/entreprises/${id}/offers`);
  // }

  // get candidats who have status approved (candidat who can pass interview)
  getCandidatByOfferAndStatus(id: number, status: string){
    return this.httpClient.get(this.baseUrl+`/getCandidatureByOfferAndStatus/${id}/${status}`);
  }

  // add new interview
  addInterview(interview: Interview, idEntreprise:number){
    const url = this.baseUrl+`/add-interview/${idEntreprise}`;
    return this.httpClient.post(url, interview);
  }

  // edit interview
  editInterview(editedInterview: Interview){
    const url = this.baseUrl+ '/edit-interview';
    return this.httpClient.put(url, editedInterview);
  }
  // delete interview
  deleteInterview(id: number){
    const url = this.baseUrl+`/delete-interview/${id}`;
    return this.httpClient.delete(url);
  }

  // edit result
  editResult(interview: Interview){
    const url = this.baseUrl+ '/edit-result-interview';
    return this.httpClient.patch(url, interview);
  }

  // edit decision hiring
  editHiring(candidat: any){
    const url = this.baseUrl + '/edit-hiring-decision'
    return this.httpClient.put(url, candidat);
  }


  //get workflow interview
  getWorkflowInterviews(offer_id: number){
    const url = this.baseUrl + `/get-workflow-interviews/${offer_id}`;
    return this.httpClient.get<WorkflowInterview[]>(url);
  }

  addOrEditWorkflowInterviews(workflow: WorkflowInterview[], offer_id: number){
    const url = this.baseUrl + `/add-workflow-interview/${offer_id}`;
    return this.httpClient.post(url, workflow);
  }

}
