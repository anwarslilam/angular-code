import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { KeycloakSecurityService } from 'src/app/services/keycloak-security.service';
import { OfferService } from 'src/app/services/offer.service';
import { InterviewService } from '../interview.service';
declare const $: any;

@Component({
  selector: 'app-list-offers',
  templateUrl: './list-offers.component.html',
  styleUrls: ['./list-offers.component.css']
})
export class ListOffersComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective, { static: false })
  public dtElement: DataTableDirective;

  dtOptions: DataTables.Settings = {};
  public offers: any = [];
  id_entreprise: number;

  public rows = [];
  public srch = [];

  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject<any>();


  constructor(
    private interviewService: InterviewService,
    private kcService: KeycloakSecurityService,
    private offerService : OfferService,
  ) { }

  ngOnInit(): void {
    this.id_entreprise = this.kcService.userInformations.entreprise;
    this.listOffersByEntreprise();
    this.dtOptions = {
      pageLength: 10,
      dom: "lrtip",
    };
  }

  // manually rendering Data table

  rerender(): void {
    $("#datatable").DataTable().clear();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
    this.offers = [];
    this.listOffersByEntreprise();
    setTimeout(() => {
      this.dtTrigger.next();
    }, 1000);
  }

  //get all offers by entreprise
  listOffersByEntreprise() {
    this.offerService.getOffers(this.id_entreprise).subscribe(
      data => {
        this.offers = data;
        this.offers = this.offers.filter(o => o.status == "open");
        this.dtTrigger.next();
        if(data != null){
          this.rows = this.offers;
          this.srch = [...this.rows];
        }
      },
      err => {
        console.error('error generated' + err);
      }
    );
  }

  //search by title
  searchTitle(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.title.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

}
