import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Candidature } from "src/app/models/candidature";
import { Certificat } from "src/app/models/certificat";
import { Diplome } from "src/app/models/diplome";
import { Experiance } from "src/app/models/experience";
import { Offer } from "src/app/models/offer";
import { Skill } from "src/app/models/skill";
import { CandidatureService } from "src/app/services/candidature.service";

@Component({
  selector: "app-candidature",
  templateUrl: "./candidature.component.html",
  styleUrls: ["./candidature.component.css"],
})
export class CandidatureComponent implements OnInit {

  candidature = new Candidature();
  offer = new Offer();
  experiences : Array<Experiance> = [];
  certificats : Array<Certificat> = [];
  skills : Array<Skill> = [];
  lstSearchskills : Array<Skill> = [];
  diplomes : Array<Diplome> = [];
  id_candidature :any;
  errorMessage : any;

  constructor(
    private candidatureService: CandidatureService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.id_candidature = this.route.snapshot.params['id'];
    this.getCandidature();
    this.getOffer();
  }

  getCandidature() {
    this.candidatureService.getCandidature(this.id_candidature).subscribe(
          data => {
            this.candidature = data;
            this.diplomes = this.candidature.diplomes;
            this.certificats = this.candidature.certificats;
            this.skills = this.candidature.skills;
            this.lstSearchskills = this.skills;
            this.experiences = this.candidature.experiences;
            console.log(this.candidature);
          },
          err => {
            this.errorMessage = err;
            console.log('error getting candidature !', err.error.message);
          })
  }

  getOffer() {
    this.candidatureService.getOfferByCandidature(this.id_candidature).subscribe(
          data => {
            this.offer = data;
            console.log(this.offer);
          },
          err => {
            this.errorMessage = err;
            console.log('error getting offer !', err.error.message);
          })
  }

  // search filter in files and docs
  searchSkills(searchValue: string) {
    this.lstSearchskills = [...this.skills].filter(res => {
      return res.name.toLowerCase().includes(searchValue.toLowerCase());
    });
  }


}
