import { Component, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { Subject } from "rxjs";
import { DataTableDirective } from "angular-datatables";
import { DatePipe } from "@angular/common";
import { OfferService } from "src/app/services/offer.service";
import { KeycloakSecurityService } from "src/app/services/keycloak-security.service";
import { Router } from "@angular/router";
import { Offer } from "src/app/models/offer";

declare const $: any;
@Component({
  selector: "app-offers",
  templateUrl: "./offers.component.html",
  styleUrls: ["./offers.component.css"],
})
export class OffersComponent implements OnInit, OnDestroy {
  @ViewChild(DataTableDirective, { static: false })
  public dtElement: DataTableDirective;
  public dtOptions: DataTables.Settings = {}; 
 
  public pipe = new DatePipe("en-US");
  public purchaseDateFormat;
  public purchaseToDateFormat;
  
  public rows = [];
  public srch = [];
  public dtTrigger: Subject<any> = new Subject();

  public allOffers: any = [];
  errorMessage: any;
  offer = new  Offer();
  id_entreprise: any;
  public tempId: any;

  domains = [
    { value: 'Web Development', label: 'Web Development'},
    { value: 'Application Development', label: 'Application Development'},
    { value: 'IT Management', label: 'IT Management'},
    { value: 'Accounts Management', label: 'Accounts Management'},
    { value: 'Support Management', label: 'Support Management'},
    { value: 'Marketing', label: 'Marketing'},
  ];

  types = [
    { value: 'fullTime', label: 'Full Time'},
    { value: 'partTime', label: 'Part Time'},
    { value: 'intership', label: 'Internship'},
    { value: 'temporary', label: 'Temporary'},
    { value: 'remote', label: 'Remote'},
    { value: 'other', label: 'Other'},
  ];

  statusList = [
    { value: 'open', label: 'Open'},
    { value: 'closed', label: 'Closed'},
  ];

  constructor(
    private offerService : OfferService,
    private toastr: ToastrService,
    private keycloakSecurityService : KeycloakSecurityService,
    private router: Router,
  ) {}

  ngOnInit() {
    $(".floating")
      .on("focus blur", function (e) {
        $(this)
          .parents(".form-focus")
          .toggleClass("focused", e.type === "focus" || this.value.length > 0);
      })
      .trigger("blur");
    
    this.id_entreprise = this.keycloakSecurityService.userInformations.entreprise;
    this.getOffers();  

    // for data table configuration
    this.dtOptions = {
      // ... skipped ...
      pageLength: 10,
      dom: "lrtip",
    };
  }

  // manually rendering Data table

  rerender(): void {
    $("#datatable").DataTable().clear();
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
    });
    this.allOffers = [];
    this.getOffers();
    setTimeout(() => {
      this.dtTrigger.next();
    }, 1000);
  }


  getOffers() {
    this.offerService.getOffers(this.id_entreprise).subscribe((data) => {
      this.allOffers = data;
      this.dtTrigger.next();
      if(data != null){
        this.rows = this.allOffers;
        this.srch = [...this.rows];
      }
      
    });
  }

  getOffer(id) {
    this.offerService.getOffer(id).subscribe(
          data => {
            this.offer = data;
          },
          err => {
            this.errorMessage = err;
            console.log('error getting offer ! ', err)
          })
  }

  // Add Provident Modal Api Call

  addOffer() { 
    this.offerService.CreateOffer(this.offer, this.id_entreprise).subscribe((data) => {
      this.router.navigate(["/entreprise/hr/offers"]);
      this.getOffers();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    });
    $("#add_offer").modal("hide");
    this.toastr.success("Offer added", "Success");
    this.offer = new Offer();
  }

  // Edit Provident Modal Api Call

  updateOffer() {
    this.offerService.UpdateOffer(this.offer.id, this.offer).subscribe((data1) => {
      this.router.navigate(["/entreprise/hr/offers"]);
      this.getOffers();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    });
    $("#edit_offer").modal("hide");
    this.toastr.success("Offer edited", "Success");
    this.offer = new Offer();
  }

  edit(value) {
    this.getOffer(value);
  }

  // Delete Provident Modal Api Call

  deleteOffer() {
    this.offerService.DeleteOffer(this.tempId).subscribe((data) => {
      this.router.navigate(["/entreprise/hr/offers"]);
      this.getOffers();
      this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        dtInstance.destroy();
      });
    });
    $("#delete_offer").modal("hide");
    this.toastr.success("Offer deleted", "Success");
  }

  //search by title
  searchTitle(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.title.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }


  //search by name
  searchCity(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.city.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }

  //search by domain

  searchDomain(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.domaine.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }

  //search by type

  searchType(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.type.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }

  //search by status

  searchStatus(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      val = val.toLowerCase();
      return d.status.toLowerCase().indexOf(val) !== -1 || !val;
    });
    this.rows.push(...temp);
  }

  //search by min experiance

  searchMinExp(val) {
    this.rows.splice(0, this.rows.length);
    let temp = this.srch.filter(function (d) {
      return d.experience >= val  || !val;
    });
    this.rows.push(...temp);
  }



  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
}
