import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Entreprise } from 'src/app/models/entreprise';
import { Offer } from 'src/app/models/offer';
import { EntrepriseService } from 'src/app/services/entreprise.service';
import { KeycloakSecurityService } from 'src/app/services/keycloak-security.service';
import { OfferService } from 'src/app/services/offer.service';

@Component({
  selector: 'app-offer-details',
  templateUrl: './offer-details.component.html',
  styleUrls: ['./offer-details.component.css']
})
export class OfferDetailsComponent implements OnInit {

  offer = new  Offer();
  entreprise = new  Entreprise();
  errorMessage: string = null;
  userInformations: any;
  id_entreprise: any;

  constructor(
    private route: ActivatedRoute,
    private offerService: OfferService,
    private entrepriseService: EntrepriseService,
    private keycloakSecurityService : KeycloakSecurityService,
  ) { }

  ngOnInit() {
    this.userInformations = this.keycloakSecurityService.userInformations;
    this.id_entreprise = this.userInformations.entreprise;
    this.getEntreprise();
    this.getOffer()
  }

  getOffer() {
    var id = this.route.snapshot.params['id'];
    this.offerService.getOffer(id).subscribe(
          data => {
            this.offer = data;
          },
          err => {
            this.errorMessage = err;
            console.log('error getting offer !', err.error.message);
          })
  }

  getEntreprise() {
    this.entrepriseService
        .getEntreprise(this.id_entreprise)
        .subscribe(
          data => {
            this.entreprise = data;
          },
          err => {
            this.errorMessage = err;
            console.log('error getting entreprise ! ', err.error.message)
          })
  }

}
