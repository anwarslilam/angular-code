import { Component, OnInit } from '@angular/core';
import { Entreprise } from 'src/app/models/entreprise';
import { EntrepriseConfigService } from 'src/app/services/entreprise-config.service';
import { EntrepriseService } from 'src/app/services/entreprise.service';
import { KeycloakSecurityService } from 'src/app/services/keycloak-security.service';
import { MessageService } from 'src/app/services/message.service';
import { ToastrService } from "ngx-toastr";

declare const $: any;

@Component({
  selector: 'app-entreprise-infos',
  templateUrl: './entreprise-infos.component.html',
  styleUrls: ['./entreprise-infos.component.css']
})
export class EntrepriseInfosComponent implements OnInit {

  userInformations: any;
  id_entreprise: any;
  entreprise = new  Entreprise();
  errorMessage: string = null;

  isAuth = false;
  keycloak: any;
  isAccepted: boolean = true;

  constructor(
    private keycloakSecurityService: KeycloakSecurityService,
    private entrepriseService: EntrepriseService,
    private messageService : MessageService,
    private entrepriseConfigService : EntrepriseConfigService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
    this.keycloak = this.keycloakSecurityService.keycloak;
    this.isAuth = this.keycloak.authenticated;
    if(this.isAuth){
      this.entrepriseConfigService.init()
      .then(data => {
        this.isAccepted = (data == "accepted") ;
  
      }).catch(err => {
        console.error('err', err);
        this.isAccepted = false;
      });
    }  
    this.userInformations = this.keycloakSecurityService.userInformations;
    this.id_entreprise = this.userInformations.entreprise;
    this.getEntreprise();
  }

  getEntreprise() {

    this.entrepriseService
        .getEntreprise(this.id_entreprise)
        .subscribe(
          data => {
            this.entreprise = data;
          },
          err => {
            this.errorMessage = err;
            console.log('errorrr ! ', err)
          })
  }

  updateEntreprise() {
    this.entrepriseService
        .UpdateEntreprise(this.entreprise.id, this.entreprise)
        .subscribe(
          response => {
            $("#edit_entreprise").modal("hide");
            this.toastr.success("Entreprise edited", "Success");
          },
          err => {
            this.errorMessage = err;
            console.log('error updating entreprise ! ', err)
          }
        );
  }

}
