import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { Candidature } from '../models/candidature';
import { Offer } from '../models/offer';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CandidatureService {

  headers: HttpHeaders =new HttpHeaders;
  options : any;
  private baseUrl = environment.baseUrl;
  constructor(
    private http: HttpClient, 
    ) { 
      this.headers.append('enctype', 'multipart/form-data');
      this.headers.append('Content-Type', 'application/json');
      this.headers.append('X-requested-with', 'XMLHttpRequest');
      this.options = {headers: this.headers};
    }

    public getCandidatures(id_offer) {
      return this.http.get<Candidature[]>(`${this.baseUrl}/GetCandidatures/`+id_offer, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public getCandidature(id) {
      return this.http.get<Candidature>(`${this.baseUrl}/GetCandidature/`+id)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public getOfferByCandidature(id) {
      return this.http.get<Offer>(`${this.baseUrl}/GetOfferByCandidature/`+id)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public CreateCandidatures(id_offer, candidatures : Array<Candidature>) {
      return this.http.post<any>(`${this.baseUrl}/createCandidatures/`+id_offer, candidatures, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        ); 
    }


    public UpdateCandidatureStatus(id : number, candidature : Candidature) {
      return this.http.put<any>(`${this.baseUrl}/updateCandidatureStatus/`+id, candidature, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public DeleteCandidature(id : number) {
      return this.http.delete<any>(`${this.baseUrl}/deleteCandidature/`+id, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

  
  
  
    private handleError(errorRes: HttpErrorResponse) {
      console.log('errorRes', errorRes)
      /*
      let errorMessage = 'an unknown error occured';
      if (!errorRes.error || !errorRes.error.error || !errorRes.error.message) {
        return throwError(errorMessage);
      }
      switch (errorRes.error.message) {
        case 'Forbidden':
          errorMessage = 'Vous n\'avez pas les droits ! ou vous n\'êtes pas logger';
          break;
        case 'EMAIL_NOT_FOUND':
          errorMessage = 'this Email does not exist';
          break;
        case 'INVALID_PASSWORD':
          errorMessage = 'this password is not correct';
          break;
  
      }
      */
      return throwError(errorRes);
    }
}
