import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Entreprise } from 'src/app/models/entreprise';
import { EntrepriseConfigService } from '../entreprise-config.service';
import { EntrepriseService } from '../entreprise.service';
import { KeycloakSecurityService } from '../keycloak-security.service';

@Injectable()
export class AfterLoginEntrepriseHrAcceptedService implements CanActivate {
  

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | Observable<boolean> | Promise<boolean> {
    if(this.keycloakSecurityService.isAuth) {
      if(this.keycloakSecurityService.isEntrepriseHR()){

        return new Promise((resolve) => {
          this.entrepriseService
          .getEntreprise(this.keycloakSecurityService.userInformations.entreprise)
          .subscribe(
            data => {
              this.entreprise = data;
              if(this.entreprise.statut == "accepted"){
                resolve(true);
              }
              else{
                this.router.navigateByUrl('entreprise/home/entreprise-home');
                resolve(false);
              }
            },
            err => {
              this.router.navigateByUrl('entreprise/home/entreprise-home');
              resolve(false);
            })
          });
      }
      else{
        this.router.navigateByUrl('');
        return false;
      }
    }
    else {
         this.router.navigateByUrl('entreprise/home/entreprise-home');
         return false;
    }
  }
  entreprise = new Entreprise();
  constructor(
    private keycloakSecurityService: KeycloakSecurityService,
    private router: Router,
    private entrepriseService : EntrepriseService
    ) { }

}
