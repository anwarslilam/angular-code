import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { tap, catchError, map } from 'rxjs/operators';
import { Critere } from '../models/critere';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CritereService {

  headers: HttpHeaders =new HttpHeaders;
  options : any;
  private baseUrl = environment.baseUrl;

  constructor(
    private http: HttpClient
    ) { 
      this.headers.append('enctype', 'multipart/form-data');
      this.headers.append('Content-Type', 'application/json');
      this.headers.append('X-requested-with', 'XMLHttpRequest');
      this.options = {headers: this.headers};
    }

    public getCriteres(id_offer) {
      return this.http.get<Critere[]>(`${this.baseUrl}/GetCriteres/`+id_offer, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public getCritere(id) {
      return this.http.get<Critere>(`${this.baseUrl}/GetCritere/`+id)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public CreateCritere(id_offer, critere) {
      return this.http.post<any>(`${this.baseUrl}/createCritere/`+id_offer, critere, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

    public DeleteCritere(id) {
      return this.http.delete<any>(`${this.baseUrl}/deleteCritere/`+id, this.options)
      .pipe(
          map(res =>res),
          catchError(this.handleError)
        );
    }

  
  
  
    private handleError(errorRes: HttpErrorResponse) {
      console.log('errorRes', errorRes)
      /*
      let errorMessage = 'an unknown error occured';
      if (!errorRes.error || !errorRes.error.error || !errorRes.error.message) {
        return throwError(errorMessage);
      }
      switch (errorRes.error.message) {
        case 'Forbidden':
          errorMessage = 'Vous n\'avez pas les droits ! ou vous n\'êtes pas logger';
          break;
        case 'EMAIL_NOT_FOUND':
          errorMessage = 'this Email does not exist';
          break;
        case 'INVALID_PASSWORD':
          errorMessage = 'this password is not correct';
          break;
  
      }
      */
      return throwError(errorRes);
    }
}
